package com.project.interests.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name = "anime")
public class Anime {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long anime_id;
	private String title;
	private Long mal_id;
	private String url;
	private String image_url;
	private boolean airing;
	private String synopsis;
	private String anime_type;
	private int episodes;
	private double score;
	private Date start_date;
	private Date end_date;
	private String rated;
	private int watch_status;
	private int episodes_watched;
	
	
	public Long getAnime_id() {
		return anime_id;
	}
	public void setAnime_id(Long anime_id) {
		this.anime_id = anime_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getMal_id() {
		return mal_id;
	}
	public void setMal_id(Long mal_id) {
		this.mal_id = mal_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public boolean isAiring() {
		return airing;
	}
	public void setAiring(boolean airing) {
		this.airing = airing;
	}
	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	public String getAnime_type() {
		return anime_type;
	}
	public void setAnime_type(String anime_type) {
		this.anime_type = anime_type;
	}
	public int getEpisodes() {
		return episodes;
	}
	public void setEpisodes(int episodes) {
		this.episodes = episodes;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public String getRated() {
		return rated;
	}
	public void setRated(String rated) {
		this.rated = rated;
	}
	public int getWatch_status() {
		return watch_status;
	}
	public void setWatch_status(int watch_status) {
		this.watch_status = watch_status;
	}
	public int getEpisodes_watched() {
		return episodes_watched;
	}
	public void setEpisodes_watched(int episodes_watched) {
		this.episodes_watched = episodes_watched;
	}
	
	
	
}
