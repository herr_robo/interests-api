package com.project.interests.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import com.project.interests.models.Anime;


public interface AnimeRepository extends JpaRepository <Anime, Long> {

}
