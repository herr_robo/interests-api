package com.project.interests.controllers;

import com.project.interests.models.Anime;
import com.project.interests.repositories.AnimeRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/anime")
public class AnimeController {
	@Autowired
	private AnimeRepository animeRepository;
	
	@GetMapping
	public List<Anime> list() {
		return animeRepository.findAll();
	}
	
	
	@GetMapping
	@RequestMapping("{id}")
	public Anime get(@PathVariable Long id) {
		return animeRepository.findById(id).get();
	}
	
	@PostMapping
	public Anime create(@RequestBody Anime anime) {
		return animeRepository.saveAndFlush(anime);		
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		animeRepository.deleteById(id);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Anime update(@PathVariable Long id, @RequestBody Anime anime) {
        // Because this is a PUT, we expect all attributes to be passed in. A PATCH would only need what is to be updated
        // TODO: Add validation that all attributes are passed in, otherwise return 400 bad payload
		Anime existingAnime = animeRepository.findById(id).get();
		BeanUtils.copyProperties(anime, existingAnime, "anime_id");
		return animeRepository.saveAndFlush(existingAnime);
	}
	
}
